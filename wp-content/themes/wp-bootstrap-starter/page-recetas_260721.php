<?php
/*
Template Name: recetas
*/
 
 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); 

$a_tags =  array('');
if( isset($_POST['search_recetas']) ){
$a_tags = $_POST['tipos_recetas'];
}
				
//var_dump(get_tags());

?>

	<div id="primary" class="home-container">
		<div id="main" class="site-main" role="main">
		
		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
				
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) : the_post();
				// get_template_part( 'template-parts/content-slider', get_post_format() );
				get_template_part( 'template-parts/content-menu-recetas', get_post_format() );
				// get_template_part( 'template-parts/content-submenu', get_post_format() );

				
				?> 

					<form method="POST" class="form-mobile">
						<input type="hidden" name="search_recetas" value="search_recetas">
						<div class="buttons-filter">
						<?php if(count($a_tags)>0){ if( in_array('diarrea-carcinoide', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[1]" type="checkbox" id="diarrea-carcinoide" class="input-diarrea-carcinoide" value="diarrea-carcinoide" <?php echo $a_check ;?>/>
						<label for="diarrea-carcinoide" class="diarrea-carcinoide"></label>
						<?php if(count($a_tags)>0){ if( in_array('esteatorrea', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[2]" type="checkbox" id="esteatorrea" class="input-esteatorrea" value="esteatorrea" <?php echo $a_check ;?>/>
						<label for="esteatorrea" class="esteatorrea"></label>
						<?php if(count($a_tags)>0){ if( in_array('diarrea-intestinal', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[3]" type="checkbox" id="diarrea-intestinal" class="input-diarrea-intestinal" value="diarrea-intestinal"<?php echo $a_check ;?> />
						<label for="diarrea-intestinal" class="diarrea-intestinal"></label>
						<?php if(count($a_tags)>0){ if( in_array('hiperglucemia', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[4]" type="checkbox" id="hiperglucemia" class="input-hiperglucemia" value="hipergluceima" <?php echo $a_check ;?>/>
						<label for="hiperglucemia" class="hiperglucemia"></label>
						<?php if(count($a_tags)>0){ if( in_array('anorexia', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[5]" type="checkbox" id="anorexia" class="input-anorexia" value="anorexia" <?php echo $a_check ;?>/>
						<label for="anorexia" class="anorexia"></label>
						<?php if(count($a_tags)>0){ if( in_array('anemia', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[6]" type="checkbox" id="anemia" class="input-anemia" value="anemia" <?php echo $a_check ;?>/>
						<label for="anemia" class="anemia"></label>
						<?php if(count($a_tags)>0){ if( in_array('liposolubles', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[7]" type="checkbox" id="liposolubles" class="input-liposolubles" value="liposolubles"<?php echo $a_check ;?> />
						<label for="liposolubles" class="liposolubles"></label>
						<?php if(count($a_tags)>0){ if( in_array('b12', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[8]" type="checkbox" id="b12" class="input-b12" value="b12" <?php echo $a_check ;?>/>
						<label for="b12" class="b12"></label>
						<?php if(count($a_tags)>0){ if( in_array('todas', $a_tags) ){ $a_check = 'checked="checked"'; }else{ $a_check = ''; } }?>
						<input name="tipos_recetas[9]" type="checkbox" id="todas" class="input-todas" value="todas" <?php echo $a_check ;?> />
						<label for="todas" class="todas"></label>		
						</div>

					<br>
					<div class="filter-container">
					<i class="fa fa-filter filtro-icono"></i>
						<input type="submit" value='' class="submit-recetas">
					</div>
					</form>


					<div class="container-doctors-recetas">

				<?php
					
					$args = array( 
						'post_type' => 'post', 
						'category_name' => 'receta', 
						'tag' => $a_tags,
						'orderby' => 'date', 
						'order' => 'DESC',
						'showposts' => 100
					);
				
					$wp_query = new WP_Query($args);
				
					if($wp_query->have_posts()) :
						while($wp_query->have_posts()) :
						$wp_query->the_post(); 
					


				?>
			
							
							<?php while (have_rows('recetas_consejos')) : the_row(); ?>
								
							<?php
							$title = get_sub_field('title', 'option');
							$description = get_sub_field('description', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('es_receta', 'option');
							?>
							<div class="drs-recetas-container">
								<div class="dr-diarrea">
									<div class="dr-diarrea-img-posts" style="background-image: url('<?php echo esc_url($image); ?>');">
									</div>
									<div class="dr-diarrea-text-posts">
									<h2><?php echo ($title) ?></h2> <br>
									<p><?php echo ($description) ?></p> 

									<?php if( $categoria == true): ?>
									<button class="ver-mas2"><a href="<?php echo get_permalink(); ?>">Ver receta</a></button>
									<?php endif; ?>
									<?php if( $categoria == false): ?>
									<button class="ver-mas2"><a href="<?php echo get_permalink(); ?>">Ver consejo</a></button>
									<?php endif; ?>
									
									</div>
								</div>
							</div>
							
							<?php
							endwhile;
				
						endwhile; endif; 

						if( $wp_query->found_posts == 0 ){
							echo '<p class="no_recetas">No se han encontrado recetas</p>';
						}

					wp_reset_query(); 
					
				?>

			
				</div>

				
				<?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-slider2', get_post_format() );

				// get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php

 get_footer();
