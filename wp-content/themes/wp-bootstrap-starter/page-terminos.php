<?php

/*
Template Name: terminos
*/
 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

		<div id="main" class="site-main" role="main">

		<div class="container-terminos">
		<h1>Términos y condiciones</h1> 
		<h2>1. Aceptación</h2> 
		<p><a href="www.nutriactivos-net.com">www.nutriactivos-net.com</a> «<b>sitio web</b>») ha sido desarrollado y es mantenido por Ipsen Pharma S.A.U., con domicilio en Torre Realia, Pça. Europa, 41-43, 7ª planta, 08908 L’Hospitalet de Llobregat (Barcelona) con CIF A20005088, teléfono 936 85 81 00, inscrita en el Registro Mercantil de Barcelona al Tomo: Folio 112, Hoja B, 7.462 (en lo sucesivo, junto con las compañías del grupo «<b>Ipsen</b>»). El acceso al sitio web y la utilización de éste están sujetos a los siguientes Términos y Condiciones, así como a todas las leyes aplicables. Al acceder al sitio web, acepta, sin limitaciones ni disquisiciones, los presentes Términos y Condiciones.</p>
		
		<h2>2. Uso de la información</h2>
		<p><a href="www.nutriactivos-net.com">www.nutriactivos-net.com</a>. ha sido creada como sitio web para alojar contenidos educativos, destinados a pacientes con tumor neuroendocrino.
		<a href="www.nutriactivosTNE.com">www.nutriactivosTNE.com</a> es una iniciativa de IPSEN, GETNE, NET España y Fundación Alicia para ofrecer a los pacientes, familiares o cuidadores, contenidos audiovisuales e infográficos para personas afectadas por un tumor neuroendocrino. La finalidad última del sitio web es por tanto ayudar a los pacientes a cubrir las necesidades nutricionales y culinarias y guiarle en cómo adaptar su alimentación a situaciones concretas, ya sea en relación a la enfermedad o por el impacto de los tratamientos.</p>
		

		<p>La información contenida en este Sitio web, destinado a todo tipo de público, se divulga exclusivamente con fines de información y no pretende en modo alguno:
		<ul>
			<li>dar una opinión médica personalizada</li>
			<li>establecer un diagnóstico</li>
			<li>sustituir a una consulta, opinión o recomendación de un profesional sanitario</li>
			<li>recomendar ni fomentar el uso de ningún producto o servicio de Ipsen</li>
		</ul>
		Resulta esencial seguir los consejos o consultar a su médico u otros profesionales sanitarios antes de utilizar cualquiera de los productos mencionados en este sitio web.</p>
		
		<p>Este sitio web no sustituye al consejo de un profesional médico. Por favor consulte a su médico en relación con cualquier consulta que pueda tener relacionada con su salud.</p>
		
		<p>Usted no podrá distribuir, modificar, divulgar, reutilizar, transmitir ni utilizar ninguna información contenida en el sitio web con fines comerciales, y debe ser consciente de que todo lo que vea o lea en este sitio web está protegido por derechos de autor a menos que se indique lo contrario y no puede utilizarse de un modo no contemplado en los presentes Términos y Condiciones. </p>
		
		<p>Excepto en el caso de que este párrafo permita lo contrario, Ipsen no afirma ni garantiza que la utilización que usted pueda hacer de los materiales mostrados en el sitio web no infringe derechos de terceros no ostentados por Ipsen ni afiliadas de ésta. Con la excepción de la autorización limitada anterior, no se le concede ninguna licencia ni derecho sobre la información, ni tampoco ningún derecho de autor de Ipsen ni de ningún tercero.</p>
		
		<p>Este sitio web puede contener o hacer referencia a información, tecnologías, productos, procesos u otros derechos que sean propiedad de Ipsen o de terceros. No se le concede a usted ninguna licencia ni ningún derecho sobre dichas marcas comerciales, patentes, secretos comerciales, tecnologías, productos, procesos y otros derechos exclusivos de Ipsen o de terceros.</p>

		<h2>3. Descargo de Responsabilidad</h2> 

		<p>Aunque Ipsen hace todos los esfuerzos razonables para garantizar que la información contenida en el sitio web es precisa y está actualizada, dicha información puede no ser precisa o contener errores tipográficos. Ipsen se reserva el derecho a realizar cambios, correcciones y mejoras en la información en cualquier momento y sin previo aviso. Ipsen no afirma ni garantiza que la información sea precisa, y declina toda responsabilidad por cualquier error u omisión en el contenido de este sitio web.</p>
		
		<p>TODA LA INFORMACIÓN SE FACILITA “TAL CUAL ES”. IPSEN NO GARANTIZA LA INTEGRIDAD NI LA PRECISIÓN DE LA INFORMACIÓN CONTENIDA EN ESTE SITIO WEB, NI TAMPOCO LOS POSIBLES USOS QUE SE HAGA DE ELLA. EN CONSECUENCIA, LOS USUARIOS DEBEN VALORAR DETENIDAMENTE LA INFORMACIÓN ANTES DE UTILIZARLA. USTED ACEPTA QUE UTILIZA ESTA INFORMACIÓN POR SU CUENTA Y RIESGO. EN LA MEDIDA PERMITIDA POR LA LEY, IPSEN, CUALQUIER COMPAÑÍA DEL GRUPO IPSEN Y CUALQUIER OTRA PARTE QUE PARTICIPE EN LA CREACIÓN, PRODUCCIÓN O PUBLICACIÓN DE ESTE SITIO WEB DECLINA TODA RESPONSABILIDAD EN RELACIÓN CON CUALQUIER DAÑO DIRECTO, INCIDENTAL, CONSECUENCIAL, INDIRECTO O PUNITIVO DERIVADO DEL ACCESO AL PRESENTE SITIO O DE SU USO O LA IMPOSIBILIDAD DE USARLO, ASÍ COMO EN RELACIÓN CON CUALQUIER ERROR U OMISIÓN EN EL CONTENIDO DEL SITIO WEB.</p>
		
		<p>Ipsen tampoco asume responsabilidad alguna, ni será considerada responsable, por ningún daño o virus que pueda infectar su equipo o cualquier propiedad que usted haya utilizado para acceder a la información o utilizarla.</p>
		
		<h2>4. La información que usted ofrece</h2>
		
		<p>A excepción de la información cubierta por la Política de Privacidad, si esta fuera aplicable, cualquier comunicación o material que envíe al sitio web, incluyendo datos, preguntas, comentarios, sugerencias, etc., es información no confidencial y sin derechos de propiedad y será tratada como tal.</p>
		
		<h2>5. Consecuencias</h2>
		
		<p>Si Ipsen descubre que usted ha incumplido cualquiera de los Términos y Condiciones contenidos en el presente documento, puede adoptar una acción correctiva inmediatamente, incluida la prohibición de utilización del Sitio web por parte del usuario, en cualquier momento y sin previo aviso. Si Ipsen se ha visto afectada o ha sufrido un daño a causa de su incumplimiento, puede, a su exclusiva discreción, exigirle el resarcimiento de dichos daños.</p>
		
		<h2>6. Revisiones</h2>
		
		<p>Ipsen puede revisar en cualquier momento los presentes Términos y Condiciones actualizando esta comunicación y notificándolo al usuario a través de medios suficientes (como un mensaje de correo electrónico o una ventana emergente, que le pedirá que preste su consentimiento a la versión modificada). Los Términos y Condiciones revisados entran en vigor 30 días naturales después de dicha actualización. Si usted no está de acuerdo con los nuevos Términos y Condiciones, debe indicarlo a través de los mecanismos propuestos por el mensaje de correo electrónico o la ventana emergente en cuestión y, como consecuencia de ello, dejar de acceder al Sitio web y de utilizarlo. Debería visitar periódicamente esta página para revisar los Términos y Condiciones en vigor en cada momento en relación con los cuales usted queda obligado.</p>
		
		<p>Ipsen se reserva el derecho a discontinuar este sitio web en cualquier momento y sin incurrir por ello en responsabilidad alguna. En el caso de que eso suceda, será advertido previamente de dicha discontinuación.</p>
		
		<h2>7. Propiedad intelectual</h2>

		<p>El contenido de este Sitio web está completamente protegido por la legislación aplicable en materia de derechos de propiedad intelectual. Ningún material contenido en este Sitio web puede ser reproducido, divulgado ni utilizado en modo alguno sin el permiso previo y por escrito de Ipsen.<p>

		<h2>8. Reacciones adversas</h2>

		<p>“El departamento de Farmacovigilancia de Ipsen desempeña un papel fundamental para evaluar el equilibrio entre los beneficios y los riesgos de nuestros medicamentos, gracias al continuo análisis de la información que reciben de pacientes, profesionales de la salud y autoridades sanitarias.</p>

		<p>Si tiene alguna consulta relacionada con posibles efectos adversos o quiere comunicar reacciones adversas de productos de Ipsen, contacte con el departamento de Farmacovigilancia de Ipsen través del teléfono 93 685 81 00, enviando un correo electrónico a <a href="pharmacovigilance.spain@ipsen.com">pharmacovigilance.spain@ipsen.com</a></p>
		
		<h2>9. Invalidez</h2>

		<p>La invalidez o inejecutabilidad de cualquiera de las disposiciones de los presentes Términos y Condiciones no afectarán a la validez ni a la ejecutabilidad de las demás disposiciones, que seguirán siendo plenamente vigentes, excepto en el caso de que la invalidez o la inejecutabilidad afecte a un término esencial de los presentes Términos y Condiciones.</p>

		<h2>10. Legislación aplicable y resolución de controversias</h2>

		<p>Los presentes Términos y Condiciones y el uso que usted haga del Sitio web se regirán por las leyes de España, excepto por lo que respecta a las disposiciones del Derecho internacional privado.</p>
		<p>Las controversias que las partes no puedan resolver de manera amistosa se someterán a los juzgados y los tribunales de Barcelona.</p>
		<p>Última actualización: Junio 2021.</p>

		</div>

		</div><!-- #main -->

<?php
get_footer();
