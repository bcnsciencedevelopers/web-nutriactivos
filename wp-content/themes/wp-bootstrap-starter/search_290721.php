<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="">
		<div id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
			<?php
			get_template_part( 'template-parts/content-menu-ok', get_post_format() );
			?>
				<h1 class="page-title-searcher"><?php printf( esc_html__( 'Search Results for: %s', 'wp-bootstrap-starter' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->
			
			<div class="search-container">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();


			if (have_rows('posts_diarrea')) : the_row(); ?>
								
							<?php
							$title = get_sub_field('title', 'option');
							$especialidad = get_sub_field('especialidad', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('categoria', 'option');
							$link = get_sub_field('link', 'option');

							?>
							<div class="drs-diarrea-container-search">
								<div class="dr-diarrea" >
									<div class="dr-diarrea-img-posts" style="background-image: url('<?php echo esc_url($image); ?>');">
									</div>
									<div class="dr-diarrea-text-posts">
									<h2><?php echo ($title) ?></h2> <br>
									<p><?php echo ($name) ?></p> <br>
									<p><i><?php echo ($especialidad) ?></i></p> <br>
									
									<button class="ver-mas"><a href="<?php echo get_permalink(); ?>">Ver más</a></button>
									
									</div>
								</div>

							</div>
							
							<?php endif; ?>

							<?php if (have_rows('posts_anemia')) : the_row(); ?>
								
								<?php
								$title = get_sub_field('title', 'option');
								$especialidad = get_sub_field('especialidad', 'option');
								$image = get_sub_field('image', 'option');
								$name = get_sub_field('name', 'option');
								$categoria = get_sub_field('categoria', 'option');
								$link = get_sub_field('link', 'option');
	
								?>
								<div class="drs-diarrea-container-search">
									<div class="dr-diarrea" >
										<div class="dr-diarrea-img-posts" style="background-image: url('<?php echo esc_url($image); ?>');">
										</div>
										<div class="dr-diarrea-text-posts">
										<h2><?php echo ($title) ?></h2> <br>
										<p><?php echo ($name) ?></p> <br>
										<p><i><?php echo ($especialidad) ?></i></p> <br>
										
										<button class="ver-mas"><a href="<?php echo get_permalink(); ?>">Ver más</a></button>
										
										</div>
									</div>
	
								</div>
								
								<?php endif; ?>
								<?php if (have_rows('posts_hiperglucemia')) : the_row(); ?>
								
								<?php
								$title = get_sub_field('title', 'option');
								$especialidad = get_sub_field('especialidad', 'option');
								$image = get_sub_field('image', 'option');
								$name = get_sub_field('name', 'option');
								$categoria = get_sub_field('categoria', 'option');
								$link = get_sub_field('link', 'option');
	
								?>
								<div class="drs-diarrea-container-search">
									<div class="dr-diarrea" >
										<div class="dr-diarrea-img-posts" style="background-image: url('<?php echo esc_url($image); ?>');">
										</div>
										<div class="dr-diarrea-text-posts">
										<h2><?php echo ($title) ?></h2> <br>
										<p><?php echo ($name) ?></p> <br>
										<p><i><?php echo ($especialidad) ?></i></p> <br>
										
										<button class="ver-mas"><a href="<?php echo get_permalink(); ?>">Ver más</a></button>
										
										</div>
									</div>
	
								</div>
								
								<?php endif; ?>
		

			<?php endwhile;

			// the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
</div>
		</div><!-- #main -->
	</section><!-- #primary -->

<?php
// get_sidebar();
get_footer();
