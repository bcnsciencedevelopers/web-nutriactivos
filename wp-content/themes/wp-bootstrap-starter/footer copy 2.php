<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<p>Una iniciativa de:</p>
	<div class="footer-menu-container">
		<div class="footer-logos">
		<?php while (have_rows('footer_logos')) : the_row(); ?>
		<?php
		$image = get_sub_field('image', 'option');
		$link = get_sub_field('link', 'option');
		?>
		<a href="<?php echo esc_url($link); ?>">
		<img src="<?php echo esc_url($image); ?>" alt="" class="logos-footer-img">
		</a>
		<?php endwhile; ?>
		</div>
		<div class="menu_footer">
		
		<?php while (have_rows('terminos')) : the_row(); ?>
		<?php
		$text = get_sub_field('text', 'option');
		$link = get_sub_field('link', 'option');
		?>
		<a href="<?php echo esc_url($link); ?>" class="terminos">
		<p class="text-footer"> <?php echo $text ?> </p>
		</a>
		<?php endwhile; ?>
		<?php while (have_rows('privacidad')) : the_row(); ?>
		<?php
		$text = get_sub_field('text', 'option');
		$link = get_sub_field('link', 'option');
		?>
		<a href="<?php echo esc_url($link); ?>" class="privacidad">
		<p class="text-footer"> <?php echo $text ?> </p>
		</a>
		<?php endwhile; ?>	
		<?php while (have_rows('cookies')) : the_row(); ?>
		<?php
		$text = get_sub_field('text', 'option');
		$link = get_sub_field('link', 'option');
		?>
		<a href="<?php echo esc_url($link); ?>" class="cookies">
		<p class="text-footer"> <?php echo $text ?> </p>
		</a>
		<?php endwhile; ?>
		</div>
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>