<?php
/*
Template Name: recetas
*/
 
 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="primary" class="home-container">
		<div id="main" class="site-main" role="main">
		
		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
				
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content-slider', get_post_format() );
				get_template_part( 'template-parts/content-menu-recetas', get_post_format() );
				// get_template_part( 'template-parts/content-submenu', get_post_format() );

				
				?> <div class="container-doctors-recetas">
					<div>
						<form action="POST">
							<!-- <label for="recetas">RECETAS</label>
							<input type="checkbox" name="recetas" id="">
							<br>
							<label for="recetas">CONSEJOS</label>
							<input type="checkbox" name="consejos" id=""> -->
							<input type="checkbox" id="diarrea-carcinoide" />
    						<label for="diarrea-carcinoide" class="diarrea-carcinoide"></label>
							<input type="submit" value="SUBMIT">
						</form>
					</div>
				<?php
					$args = array( 
						'post_type' => 'post', 
						'category_name' => 'receta, consejo', 
						// 'category_name' => array('diarrea+important'),
						// 'category_name' => array(['diarrea'] , ['important']), 
						// 'category_name' => array('diarrea , important'), 
						'orderby' => 'date', 
						'order' => 'ASC',
						'showposts' => 10);
				
					$wp_query = new WP_Query($args);
				
					if($wp_query->have_posts()) :
						while($wp_query->have_posts()) :
						$wp_query->the_post(); 
						
				?>
				
							<?php while (have_rows('recetas_consejos')) : the_row(); ?>
								
							<?php
							$title = get_sub_field('title', 'option');
							$description = get_sub_field('description', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('es_receta', 'option');
							?>
							<div class="drs-recetas-container">
								<div class="dr-diarrea">
									<div class="dr-diarrea-img-posts" style="background-image: url('<?php echo esc_url($image); ?>');">
									</div>
									<div class="dr-diarrea-text-posts">
									<h2><?php echo ($title) ?></h2> <br>
									<p><?php echo ($description) ?></p> 

									<?php if( $categoria == true): ?>
									<button class="ver-mas2"><a href="<?php echo get_permalink(); ?>">Ver receta</a></button>
									<?php endif; ?>
									<?php if( $categoria == false): ?>
									<button class="ver-mas2"><a href="<?php echo get_permalink(); ?>">Ver consejo</a></button>
									<?php endif; ?>
									
									</div>
								</div>
							</div>
							
							<?php endwhile; ?>
				
				
					<?php   endwhile; endif; 
					wp_reset_query(); 
					
				?>

			
				</div>

				
				<?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-slider2', get_post_format() );

				// get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php

 get_footer();
