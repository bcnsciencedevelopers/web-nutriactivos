<div class="menu-container-submain-mobile">
	<div class="title-submain">
		<h1><b class="important-submain">Tumores neuroendocrinos</b> <br>
		Cómo adaptar la alimentación y <br>
		las técnicas de cocinado según <br>
		 presencia de síntomas</div></h1>
		<div class="submenu-icons">
				<?php while (have_rows('submenu')) : the_row(); ?>
					<?php
					$title = get_sub_field('link', 'option');
					$image = get_sub_field('image', 'option');
					$link = get_sub_field('link', 'option');
					?>
				<a href="<?php echo esc_url($link); ?>">
				<img class="sub-img-mobile" src="<?php echo esc_url($image); ?>" alt="First slide">

				</a>
				<?php endwhile; ?>	
				</div>
				</div>

<div class="menu-container-submain-web">
	<div class="title-submain">
		<h1><b class="important-submain">Tumores neuroendocrinos</b> <br>
		Cómo adaptar la alimentación y las técnicas <br>
		de cocinado según presencia de síntomas</div></h1>
		
				<?php while (have_rows('submenu')) : the_row(); ?>
					<?php
					$title = get_sub_field('link', 'option');
					$image = get_sub_field('image', 'option');
					$link = get_sub_field('link', 'option');
					?>
				<a href="<?php echo esc_url($link); ?>">
				<img class="sub-img-web" src="<?php echo esc_url($image); ?>" alt="First slide">
				</a>
				<?php endwhile; ?>	
		
				</div>