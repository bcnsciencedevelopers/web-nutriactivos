<div id="carouselExampleIndicators" class="carousel slide slider-mobile" data-ride="carousel">
  <ol class="carousel-indicators">
	  <?php while (have_rows('slider_header_mobile')) : the_row(); ?>
	  	<?php $counter = get_row_index(); ?>
		  <?php $counter = get_row_index(); ?>
    <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $counter -1; ?>" class="<?php if (get_row_index() == 1) echo 'active'; ?>"></li>
	<?php endwhile ?>
  </ol>
  <div class="carousel-inner">
	  <?php while (have_rows('slider_header_mobile')) : the_row(); ?>
    <div class="carousel-item <?php if(get_row_index() == 1) echo 'active'; ?>">
		<?php
		$image = get_sub_field('image', 'option');
		?>
      <img class="d-block w-100" src="<?php echo esc_url($image); ?>" alt="First slide">
    </div>
	<?php endwhile; ?>
  </div>

		
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>

<div id="carouselExampleIndicators2" class="carousel slide slider-web" data-ride="carousel">
  <ol class="carousel-indicators">
	  <?php while (have_rows('slider_header')) : the_row(); ?>
	  	<?php $counter = get_row_index(); ?>
		  <?php $counter = get_row_index(); ?>
    <li data-target="#carouselExampleIndicators2" data-slide-to="<?php echo $counter -1; ?>" class="<?php if (get_row_index() == 1) echo 'active'; ?>"></li>
	<?php endwhile ?>
  </ol>
  <div class="carousel-inner">
	  <?php while (have_rows('slider_header')) : the_row(); ?>
    <div class="carousel-item <?php if(get_row_index() == 1) echo 'active'; ?>">
		<?php
		$image = get_sub_field('image', 'option');
		?>
      <img class="d-block w-100" src="<?php echo esc_url($image); ?>" alt="First slide">
    </div>
	<?php endwhile; ?>
  </div>

		
  <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>

