<?php // Upgrade to ACF Theme Code Pro for repeater field support. ?>
<?php // Visit http://www.hookturn.io for more information. ?>
<div class="diarrea-container-mobile">
<?php while (have_rows('icon-submenu')) : the_row(); ?>
		<?php
		$image = get_sub_field('image', 'option');
		$text = get_sub_field('text', 'option');
		?>
	<div class="title-icon-submenu">
	<img src="<?php echo esc_url($image); ?>" alt="" class="diarrea-img">
	<h1><?php echo ($text); ?></h1>
	<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor
	incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
	nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. <br> <br>
	Loriti ut hil id quatur, sequias maximporis arum fugitat fugia quid magnatio.
	Nequi tem atur sincto temo corro ilic tem quatibus ma id magnita quo corectur
	aut porpore optatem hicimoluptas venduscitiis eatetur, te sim quisit, quibusda
	doluptin cupta autempores conem cus.
	</p>
	</div>
<?php endwhile; ?>	
</div>
