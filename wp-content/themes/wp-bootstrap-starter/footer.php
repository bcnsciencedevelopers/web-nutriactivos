<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<p>Una iniciativa de:</p>
	<div class="footer-menu-container">
		<div class="footer-logos">
			<a href="https://www.ipsen.com/spain/" target="__blank">
				<img src="http://marketing-clm.com/nutriactivos/wp-content/uploads/2021/07/logo-ipsen-footer.png" alt="" class="logos-footer-img">
			</a>

			<a href="https://getne.org/" target="__blank">
				<img src="http://marketing-clm.com/nutriactivos/wp-content/uploads/2021/07/logo-getne-footer.png" alt="" class="logos-footer-img">
			</a>

			<a href="https://netespana.org/" target="__blank">
				<img src="http://marketing-clm.com/nutriactivos/wp-content/uploads/2021/07/logo-NET-footer.png" alt="" class="logos-footer-img">
			</a>

			<a href="https://www.alicia.cat/es/" target="__blank">
				<img src="http://marketing-clm.com/nutriactivos/wp-content/uploads/2021/07/logo-fund.-alicia-footer.png" alt="" class="logos-footer-img">
			</a>
		</div>
		<div class="menu_footer">	
		<a href="http://marketing-clm.com/nutriactivos/terminos-y-condiciones-de-uso" class="terminos">
		<p class="text-footer"> Términos y condiciones de uso</p>

		<a href="http://marketing-clm.com/nutriactivos/politica-privacidad" class="privacidad">
		<p class="text-footer"> Política de privacidad</p>

		<a href="http://marketing-clm.com/nutriactivos/politica-de-cookies" class="cookies">
		<p class="text-footer"> Política de cookies</p>
		</a>
		</div>
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>