<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); 
?>

		<div id="main" class="site-main" role="main">

			
		
		<?php
		while ( have_posts() ) : the_post();
		
			get_template_part( 'template-parts/content-menu-ok', get_post_format() );

			    // the_post_navigation();
				
				while (have_rows('posts_diarrea')) : the_row(); ?>
								
							<?php
							$the_link = get_permalink();
							$description = get_sub_field('description', 'option');
							$title = get_sub_field('title', 'option');
							$especialidad = get_sub_field('especialidad', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('categoria', 'option');
							$recursos_relacionados = get_sub_field('recursos_relacionados', 'option');
							?>
							<div class="rutas">
								<!-- <p>Tumores neuroendocrinos / Diarrea / Diarrea S. Carcinoide (y flusing)</p> -->
								<p><?php echo("Tumores neuroendocrinos / Diarrea / "); ?> <span class="bold"><?php echo ($title) ?></span></p>
								</div>
							<div class="title-icon-submenu2">
								<p><?php echo ($description) ?> </p>
								</div>
						
				
						<div class="container-doctors-diarrea">
							<div class="dr-important">
									<div class="caja-img">
										<img src="" alt="">
									<div class="img-dr-important" style="background-image: url('<?php echo esc_url($image); ?>');">
									<i class="fa fa-play-circle"></i></div>
									<!-- <i class="fa fa-play-circle"></i> -->
								</div>
								<div class="caja-texto">
									<h2><?php echo ($title) ?> <a href="#post<?php echo $the_link; ?>"></a> </h2> <br>
									<p><?php echo ($name) ?></p> <br>
									<p><i><?php echo ($especialidad) ?></i></p>
							</div>
							</div>
							<?php
								$featured_posts = get_sub_field('recursos_relacionados');
								if( $featured_posts ): ?>
									<div class="descarga-recursos">
										<p><b class="bold">Descarga</b> los siguientes <b class="bold">recursos</b> para saber más:</p>
									</div>
								<div class="recursos-container d-flex align-content-between flex-wrap">
									<?php foreach( $featured_posts as $featured_post ): 
										$permalink = get_permalink( $featured_post->ID );
										$title = get_the_title( $featured_post->ID );
										$custom_field = get_field( 'field_name', $featured_post->ID );
										?>
										<div class="recursos-dr">
										<div class="caja-img">
											<div class="img-dr-recursos" style="background-image: url('<?php echo get_the_post_thumbnail_url( $featured_post->ID, 'thumbnail' ) ?>');">
											</div>
										</div>

										<div class="caja-texto-3">
											<div class="title-recursos">
											<a href="<?php echo esc_url( $permalink );; ?>">
											<h2><?php echo esc_html( $title ); ?></h2></a> <br>
										</div>
										</div>
									
									</div>
									<?php endforeach; ?>
									</div>
								<?php endif; ?>

						
						

							
								<div class="volver">
									<button class="volver-btn"><a href="<?php echo esc_url( home_url( '/diarrea' )); ?>">← volver</a></button>
									</div>	
						</div>
					
						</div>
						</div>
							</div>
							<?php endwhile; ?>

							<!-- anemia -->
							<?php 
							while (have_rows('posts_anemia')) : the_row(); ?>
								
							<?php
							$the_link = get_permalink();
							$description = get_sub_field('description', 'option');
							$title = get_sub_field('title', 'option');
							$especialidad = get_sub_field('especialidad', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('categoria', 'option');
							$recursos_relacionados = get_sub_field('recursos_relacionados', 'option');
							?>
							<div class="rutas">
								<p><?php echo("Tumores neuroendocrinos / Anemia / "); ?> <span class="bold"><?php echo ($title) ?></span></p>
								</div>
							<div class="title-icon-submenu2">
								<p><?php echo ($description) ?> </p>
								</div>
						
				
						<div class="container-doctors-diarrea">
							<div class="dr-important">
									<div class="caja-img">
										<img src="" alt="">
									<div class="img-dr-important" style="background-image: url('<?php echo esc_url($image); ?>');">
									<i class="fa fa-play-circle"></i></div>
									<!-- <i class="fa fa-play-circle"></i> -->
								</div>
								<div class="caja-texto">
									<h2><?php echo ($title) ?> <a href="#post<?php echo $the_link; ?>"></a> </h2> <br>
									<p><?php echo ($name) ?></p> <br>
									<p><i><?php echo ($especialidad) ?></i></p>
							</div>
							</div>
							<?php
								$featured_posts = get_sub_field('recursos_relacionados');
								if( $featured_posts ): ?>
									<div class="descarga-recursos">
										<p><b class="bold">Descarga</b> los siguientes <b class="bold">recursos</b> para saber más:</p>
									</div>
								<div class="recursos-container d-flex align-content-between flex-wrap">
									<?php foreach( $featured_posts as $featured_post ): 
										$permalink = get_permalink( $featured_post->ID );
										$title = get_the_title( $featured_post->ID );
										$custom_field = get_field( 'field_name', $featured_post->ID );
										?>
										<div class="recursos-dr">
										<div class="caja-img">
											<div class="img-dr-recursos" style="background-image: url('<?php echo get_the_post_thumbnail_url( $featured_post->ID, 'thumbnail' ) ?>');">
											</div>
										</div>

										<div class="caja-texto-3">
											<div class="title-recursos">
											<a href="<?php echo esc_url( $permalink );; ?>">
											<h2><?php echo esc_html( $title ); ?></h2></a> <br>
										</div>
										</div>
									
									</div>
									<?php endforeach; ?>
									</div>
								<?php endif; ?>

						
						

							
								<div class="volver">
									<button class="volver-btn"><a href="<?php echo esc_url( home_url( '/anemia' )); ?>">← volver</a></button>
									</div>	
						</div>
					
						</div>
						</div>
							</div>
							<?php endwhile; ?>

							<?php while (have_rows('recetas_consejos')) : the_row(); ?>
								
							<?php
							$description = get_sub_field('description', 'option');
							$title = get_sub_field('title', 'option');
							$ingredientes = get_sub_field('ingredientes', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('categoria', 'option');
							$pdf = get_sub_field('pdf', 'option');
							$observaciones = get_sub_field('observaciones', 'option');
							$racion = get_sub_field('racion', 'option');
							?>
							<div class="title-icon-submenu3">
								<p><?php echo ($title) ?> </p>
								</div>
						<div class="container-doctors-diarrea">
							<div class="dr-important">
									<div class="caja-img">
										<img src="" alt="">
									<div class="img-dr-important" style="background-image: url('<?php echo esc_url($image); ?>');">
									<!-- <i class="fa fa-play-circle"></i></div> -->
									<!-- <i class="fa fa-play-circle"></i> -->
								</div>
								<div class="caja-texto2">
									<div class="ingredientes">
										<p class="title-ie">Ingredientes para <?php echo $racion ?> ración/es:</p> <br>
										<ul>
										<?php while (have_rows('ingredientes')) : the_row(); 
										$texto = get_sub_field('texto', 'option');
										?>
										
											<li>
											<p class="list-text"><?php echo $texto ?></p>	
											</li>

	
										<?php endwhile; ?>
										</ul>
									</div>
									<div class="elaboracion">
										<p class="title-ie"><?php echo "Elaboración:" ?></p> <br>
										<ul>
										<?php while (have_rows('elaboracion')) : the_row(); 
										$texto = get_sub_field('texto', 'option');
										?>
											<li>
											<p class="list-text"><?php echo $texto ?></p>	
											</li>
										<?php endwhile; ?>
										</ul>
										<br>
										<p class="title-ie"><?php echo "Observaciones y recomendaciones:" ?></p> <br>
											<p class="list-text"><?php echo ($observaciones); ?></p>	
										<br>
										<p class="title-ie"><?php echo "Valoración nutricional orientativa por ración:" ?></p> <br>

											<?php while (have_rows('tabla')) : the_row(); 
										$energia = get_sub_field('energia', 'option');
										$proteinas = get_sub_field('proteinas', 'option');
										$grasas = get_sub_field('grasas', 'option');
										$carbohidratos = get_sub_field('carbohidratos', 'option');
										?>
											<table class="table table-striped">
												<thead>
													<tr>
													<th scope="col">Energía</th>
													<th scope="col">Proteínas</th>
													<th scope="col">Grasas</th>
													<th scope="col">Carbohidratos</th>
													</tr>
												</thead>
												<tbody>
													<tr>
													<td><p class="list-text"><?php echo $energia ?></p>	</td>
													<td><p class="list-text"><?php echo $proteinas ?></p>	</td>
													<td><p class="list-text"><?php echo $grasas ?></p>	</td>
													<td><p class="list-text"><?php echo $carbohidratos ?></p>	</td>

													</tr>
													<tr>
												</tbody>
												</table>
											
										
										<?php endwhile; ?>
									</div>


								</div>
								<div class="descargar">
								<a href="<?php echo esc_url($pdf); ?>" download>
								<img src="http://marketing-clm.com/nutriactivos/wp-content/uploads/2021/07/boton-descarga.png" alt="" class="descargar-img">
								</a>
								</div>
							
							</div>
							<?php if (have_rows('recursos_relacionados')) : the_row() ?>
							<div class="descarga-recursos">
								<p><b class="bold">Descarga</b> los siguientes <b class="bold">recursos</b> para saber más:</p>
							</div>
							<?php endif; ?>
							<div class="recursos-container d-flex align-content-between flex-wrap">
							<?php while (have_rows('recursos')) : the_row(); 
							$title = get_sub_field('title', 'option');
							$subtitle = get_sub_field('subtitle', 'option');
							$image = get_sub_field('image', 'option');
							$pdf = get_sub_field('pdf', 'option');
	
							?>
							<div class="recursos-dr">
									<div class="caja-img">
									<div class="img-dr-recursos" style="background-image: url('<?php echo esc_url($image); ?>');">
									</div>
									<!-- <i class="fa fa-play-circle"></i> -->
								</div>
								<div class="caja-texto-3">
									<div class="title-recursos">
									<h2><?php echo ($title) ?></h2> <br>
									<p><?php echo ($subtitle) ?></p> <br>
									</div>
									<div class="descargar">
									<a href="<?php echo esc_url($pdf); ?>" download>
									<img src="http://marketing-clm.com/nutriactivos/wp-content/uploads/2021/07/boton-descarga.png" alt="" class="descargar-img">
									</a>
									</div>
							</div>
							</div> <!-- final div recursos container -->

							<?php   endwhile; ?>

							
							</div>
						
							</div>
							<?php endwhile; ?>

							<!-- sabias que-->
							<?php 
							while (have_rows('posts_sabiasque2')) : the_row(); ?>
							<?php
							$editor = get_sub_field('editor', 'option');
	
							?>
								

							<div class="rutas">
								<p><?php echo("Tumores neuroendocrinos / Anemia / "); ?> <span class="bold"><?php echo ($title) ?></span></p>
								</div>
							<div class="title-icon-submenu2">
								<p><?php echo ($text) ?> </p>
								</div>
						
				
						<div class="container-doctors-diarrea">
							<div class="dr-important">
									<div class="caja-img">
										<img src="" alt="">
									<div class="img-dr-important" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
									<!-- <i class="fa fa-play-circle"></i></div> -->
									<!-- <i class="fa fa-play-circle"></i> -->
								</div>
								<div class="caja-texto">
									<h2><?php echo ($title) ?> <a href="#post<?php echo $the_link; ?>"></a> </h2> <br>
									<p><?php echo ($name) ?></p> <br>
									<p><i><?php echo ($especialidad) ?></i></p>
							</div>
							</div>
							<?php
								$featured_posts = get_sub_field('recursos_relacionados');
								if( $featured_posts ): ?>
									<div class="descarga-recursos">
										<p><b class="bold">Descarga</b> los siguientes <b class="bold">recursos</b> para saber más:</p>
									</div>
								<div class="recursos-container d-flex align-content-between flex-wrap">
									<?php foreach( $featured_posts as $featured_post ): 
										$permalink = get_permalink( $featured_post->ID );
										$title = get_the_title( $featured_post->ID );
										$custom_field = get_field( 'field_name', $featured_post->ID );
										?>
										<div class="recursos-dr">
										<div class="caja-img">
											<div class="img-dr-recursos" style="background-image: url('<?php echo get_the_post_thumbnail_url( $featured_post->ID, 'thumbnail' ) ?>');">
											</div>
										</div>

										<div class="caja-texto-3">
											<div class="title-recursos">
											<a href="<?php echo esc_url( $permalink );; ?>">
											<h2><?php echo esc_html( $title ); ?></h2></a> <br>
										</div>
										</div>
									
									</div>
									<?php endforeach; ?>
									</div>
								<?php endif; ?>

						
						

							
								<div class="volver">
									<button class="volver-btn"><a href="<?php echo esc_url( home_url( '/anemia' )); ?>">← volver</a></button>
									</div>	
						</div>
					
						</div>
						</div>
							</div>
							<?php endwhile; ?>


							<!--sabias que 2-->

							<?php while (have_rows('posts_sabiasque')) : the_row(); ?>
								
							<?php
							$description = get_sub_field('description', 'option');
							$editor = get_sub_field('editor', 'option');
							?>
							<div class="title-icon-submenu3">
								<p><?php the_title();?> </p>
								</div>
						<div class="container-doctors-diarrea">
							<div class="dr-important">
									<div class="caja-img">
										<img src="" alt="">
									<div class="img-dr-important" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
									<!-- <i class="fa fa-play-circle"></i></div> -->
									<!-- <i class="fa fa-play-circle"></i> -->
									</div>
								</div>
								<div class="caja-texto-4">
								<?php echo($editor) ?>
								</div>	
							
							</div>
							
							<div class="descarga-recursos">
								<p><b class="bold">Descubre</b> más:</p>
							</div>
							
							<?php echo do_shortcode('[smart_post_show id="768"]'); ?>

						
							</div>
							<?php endwhile; ?>


							
		<?php endwhile; // End of the loop.
		?>
	
		</div><!-- #main -->

<?php
// get_sidebar();
get_footer();
