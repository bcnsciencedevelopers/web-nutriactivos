<?php
/*
Template Name: hiperglucemia
*/
 
 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="primary" class="home-container">
		<div id="main" class="site-main" role="main">
		
		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
				
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php
			endif;
			
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				// get_template_part( 'template-parts/content-slider', get_post_format() );
				get_template_part( 'template-parts/content-menu-ok', get_post_format() );
				get_template_part( 'template-parts/content-anemia', get_post_format() );
		
					
				?>
				
				<div class="container-doctors-diarrea">
				<!-- MOSTRAR POST -->
				<?php
     
					$args = array( 
						'post_type' => 'post', 
						'category_name' => 'hiperglucemia', 
						// 'category_name' => array('diarrea+important'),
						// 'category_name' => array(['diarrea'] , ['important']), 
						// 'category_name' => array('diarrea , important'), 
						'orderby' => 'date', 
						'order' => 'DESC',
						'showposts' => 1);
				
					$wp_query = new WP_Query($args);
				
					if($wp_query->have_posts()) :
						while($wp_query->have_posts()) :
						$wp_query->the_post(); 
						
				?>
				
							<?php while (have_rows('posts_hiperglucemia')) : the_row(); ?>
								
							<?php
							$title = get_sub_field('title', 'option');
							$especialidad = get_sub_field('especialidad', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('categoria', 'option');
							$link = get_sub_field('link', 'option');
							$link_url = $link['url'];
							?>
							<div class="dr-important">
									<div class="caja-img">
									<div class="img-dr-important" style="background-image: url('<?php echo esc_url($image); ?>');">
									<<a href="<?php echo esc_url($link_url); ?>" target="__blank">
									<i class="fa fa-play-circle"></i>
									</a></div>
									
								</div>
								<div class="caja-texto">
									<h2><?php echo ($title) ?></h2> <br>
									<p><?php echo ($name) ?></p> <br>
									<p><i><?php echo ($especialidad) ?></i></p>
							</div>
							</div>
							<div class="descarga-recursos">
								<p><b class="bold">Descarga</b> los siguientes <b class="bold">recursos</b> para saber más:</p>
							</div>
							
							<?php
								$featured_posts = get_sub_field('recursos_relacionados');
								if( $featured_posts ): ?>
								<div class="recursos-container">
									<?php foreach( $featured_posts as $featured_post ): 
										$permalink = get_permalink( $featured_post->ID );
										$title = get_the_title( $featured_post->ID );
										$custom_field = get_field( 'field_name', $featured_post->ID );
										?>
										<div class="recursos-dr">
										<div class="caja-img">
											<div class="img-dr-recursos" style="background-image: url('<?php echo get_the_post_thumbnail_url( $featured_post->ID, 'thumbnail' ) ?>');">
											</div>
										</div>

										<div class="caja-texto">
											<div class="title-recursos">
											<a href="<?php echo esc_url( $permalink );; ?>">
											<h2><?php echo esc_html( $title ); ?></h2></a> <br>
										</div>
										</div>
									
									</div>
									<?php endforeach; ?>
									</div>
								<?php endif; ?>
							
							<?php endwhile; ?>
					<?php   endwhile; endif; 
					wp_reset_query(); 
					
				?>
				
				<div class="volver">
				<button class="volver-btn"><a href="<?php echo esc_url( home_url( '/' )); ?>">← volver</a></button>
				</div>			
			</div>
				<?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				// get_template_part( 'template-parts/content', get_post_format() );
					
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>


		</div><!-- #main -->

	</div><!-- #primary -->

<?php

get_footer();
