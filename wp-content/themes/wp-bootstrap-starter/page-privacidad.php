<?php

/*
Template Name: privacidad
*/
 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

		<div id="main" class="site-main" role="main">

		<div class="container-terminos">
		<h1>Política de privacidad</h1> 
		<p>Ipsen tiene el compromiso de proteger sus datos personales de conformidad con la legislación vigente en materia de protección de datos, incluyendo el REGLAMENTO (UE) 2016/679 DEL PARLAMENTO EUROPEO Y DEL CONSEJO de 27 de abril de 2016, relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos (RGPD), y la legislación local aplicable a la materia y que regula el almacenamiento, tratamiento, acceso y transferencia de datos personales. </p>
		<p>Esta Política de Privacidad describe cómo IPSEN PHARMA S.A.U., con domicilio en Torre Realia, Pça. Europa, 41-43, 7ª planta, 08908 L’Hospitalet de Llobregat (Barcelona), provista con CIF A20005088 («<b class="bold">Ipsen</b>»), responsable del tratamiento de los datos, puede recoger, almacenar y usar la Información Personal de las personas físicas («<b class="bold">Usuarios</b>») que visiten este sitio web (el «<b class="bold">Sitio Web</b>»).</p>
		<p>Con la finalidad de cumplir con la normativa vigente en cada momento, <b class="bold">esta Política puede cambiar, ser modificada o actualizada de cualquier otra forma en cualquier momento y sin notificación previa</b>, por lo que le invitamos a que consulte esta página periódicamente.</p>
		<h2>1. Información Personal que se recoge</h2> 
		<h3>Datos facilitados voluntariamente por el Usuario</h3>
		<p>Ipsen puede recoger Información Personal cuando se registre como Usuario, incluyendo su nombre, dirección de correo electrónico, ciudad y país de residencia (“<b class="bold">Información Personal</b>”). Si quiere recibir información de Ipsen o contactar con Ipsen, estas categorías de Información Personal son obligatorias. </p>	
		<p>Cuando se registra como Usuario, claramente identificamos qué información se necesita para el registro y qué información es opcional y puede facilitar discrecionalmente. </p>
		<h3>Datos de navegación</h3>
		<p>En el curso normal de sus operaciones, los sistemas de información y procedimientos de software usados para el funcionamiento del Sitio Web, recogen datos personales cuya transmisión está implícita cuando se usan protocolos de comunicación por Internet. 
			Dichos datos incluyen: dirección IP, el tipo de navegador usado, el sistema operativo, el nombre de dominio y la dirección de sitios webs desde o hacia los que se accede, información acerca de las páginas visitadas por los Usuarios en la web, tiempo de acceso y tiempo que se ha estado en cada página, análisis del camino interno y otros parámetros relativos al sistema operativo y entorno de IT del Usuario.
		</p>
		<p>Dichos datos técnicos/de IT se reúnen y utilizan exclusivamente de forma agregada y anónima, y pueden utilizarse para determinar responsabilidades en caso de presuntos delitos de IT o electrónicos que ocasionen un daño al Sitio Web.</p>
		<h2>2. Uso de la información personal</h2>
		<p>Su Información Personal sólo será tratada con base en el interés legítimo de Ipsen, para tener una mejor interacción con usted, así como para hacer el Sitio Web más útil, por lo que su Información Personal puede usarse para:</p>
		<ul>
			<li>Responder a sus solicitudes y preguntas;</li>
			<li>Personalizar su experiencia;</li>
			<li>Llevar a cabo análisis del Sitio Web y medir su rendimiento;</li>
			<li>Trazar y monitorizar efectos no deseados y otras actividades relativas a la farmacovigilancia; y</li>
			<li>Mantener el Sitio Web, incluyendo fines de seguridad.</li>
		</ul>
		<p>Su Información Personal no será vendida, compartida ni distribuida de ningún otro modo a terceros sin su consentimiento, a excepción de los casos en que estemos obligados legalmente a ello, o en obediencia a un mandato judicial o una normativa gubernamental, o si esa revelación es necesaria para colaborar en cualquier investigación o procedimiento penal o de cualquier otro tipo legal, aquí o en otros países.</p>
		<h2>3. Receptores de la Información Personal</h2>
		<p>Para las finalidades descritas anteriormente, Ipsen podría necesitar compartir su Información Personal con:</p>
		<ul>
			<li><b class="bold">Ipsen y sus filiales;</b></li>
			<li><b class="bold">Proveedores, proveedores de servicios o terceros</b> que actúen siguiendo instrucciones de Ipsen con fines de Hosting del Sitio Web, análisis de datos, cumplimiento de órdenes, tecnología de la información y provisión de infraestructura relacionada, servicios al cliente, mensajería electrónica, auditoría, etc.;</li>
			<li><b class="bold">Autoridades:</b> si así lo exige la legislación aplicable, incluyendo legislación de fuera de su país de residencia;</li>
			<li><b class="bold">Adquirentes potenciales y otros terceros interesados</b> en los casos de fusión, operaciones de reestructuración tales como adquisiciones, joint ventures, escisiones o desinversiones; <br>
			En cualquier caso, Ipsen exigirá a dichos terceros:
				<ul>
					<li>Un compromiso de que van a cumplir con la legislación en materia de protección de datos y con los principios de esta Política;</li>
					<li>Que sólo tratarán los datos para los fines descritos en esta Política;</li>
					<li>Que implementarán medidas técnicas y organizativas adecuadas para proteger la integridad y confidencialidad de la Información Personal.</li>
				</ul>
			</li>
		</ul>
		<h2>4. Transferencias de Información Personal</h2>
		<p>Ipsen es un grupo biofarmacéutico global con filiales, socios y subcontratistas localizados en todo el mundo. Es por ello por lo que Ipsen puede tener necesidad de transferir su Información Personal a otras jurisdicciones incluyendo desde el Espacio Económico Europeo hasta fuera del mismo, a países que no ofrecen el mismo nivel de protección que el país en el que usted se encuentra.</p>
		<p>En aquellos casos en que Ipsen necesite transferir su Información Personal fuera de la Unión Europea, se asegurará de que se han implementado garantías adecuadas, como exige la legislación sobre protección de datos (incluyendo cláusulas tipo de protección de datos adoptadas por la Comisión si fueran aplicables).</p>
		<h2>5. Plazo de conservación de la Información Personal</h2>
		<p>Su información personal será tratada y no se conservará durante más de 3 años a partir del evento que suceda más tarde: (i) su recogida o (ii) el último contacto que mantengamos con usted.</p>
		<h2>6. Seguridad</h2>
		<p>Su Información Personal se almacenará en servidores seguros y se mantendrá en la más estricta confidencialidad. Usted es el responsable de proteger su contraseña y la información de su cuenta.</p>
		<h2>7. Derecho de acceso, rectificación y limitación</h2>
		<p>Tiene los siguientes derechos en relación con su Información Personal dependiendo de las circunstancias y de la legislación aplicable:</p>
		<div class="table-responsive-xl">
		<table class="table table-striped">
			<thead>
				<tr>
				<th scope="col" style="width: 5%; border-top: none;"></th>
				<th scope="col" style="width: 30%; border-top: none;">Derecho</th>
				<th scope="col" style="border-top: none;">¿Qué significa?</th>
				</tr>
			</thead>
			<tbody>
				<tr>
				<th scope="row">a.  </th>
				<td>Derecho de Acceso</td>
				<td>Tiene derecho a acceder a su Información Personal tratada por Ipsen.</td>
				</tr>
				<tr>
				<th scope="row">b. </th>
				<td>Derecho de rectificación</td>
				<td>Tiene derecho a que se rectifique su Información si no es correcta o es incompleta.</td>
				</tr>
				<tr>
				<th scope="row">c.</th>
				<td>Derecho de supresión</td>
				<td>Conocido también como derecho al olvido. De manera simple, le permite solicitar el borrado de su información cuando no exista una razón convincente por parte de Ipsen para seguir usándola. No es un derecho general, tiene excepciones.</td>
				</tr>
				<tr>
				<th scope="row">d.</th>
				<td>Derecho a la limitación del tratamiento</td>
				<td>Tiene derecho a bloquear el uso de su Información en ciertas circunstancias. Cuando haya limitado el tratamiento, Ipsen podrá continuar almacenando su Información, pero no usándola con posterioridad.</td>

				</tr>
				<tr>
				<th scope="row">e.</th>
				<td>Derecho a la portabilidad</td>
				<td>Tiene derecho a recibir su Información en un formato estructurado, de
					uso común y lectura mecánica, en ciertas circunstancias.
					</td>

				</tr>
				<tr>
				<th scope="row">f.</th>
				<td>Derecho de oposición</td>
				<td>Tiene derecho a oponerse a ciertos tratamientos en ciertas circunstancias.</td>

				</tr>
			</tbody>
			</table>
		</div>	
		<h3>Si quiere ejercitar cualquiera de estos derechos, por favor, envíenos una solicitud a <a href="mailto:dataprivacy@ipsen.com" targer="__blank">dataprivacy@ipsen.com</a></h3>
		<p>Si no le satisface la respuesta, o piensa que el tratamiento de su Información Personal no cumple con la legislación en materia de protección de datos, puede elevar una queja a la autoridad de control competente.</p>
		<h2>9. Redireccionamiento a sitios externos</h2>
		<p>Para posibilitar la incorporación de funciones de Redes Sociales directamente en el Sitio Web (p.ej. la función “Me gusta” en Facebook, o la función “Seguir” en Twitter), podría encontrar en este Sitio Web algunas herramientas especiales como plugin sociales.
		Todos los plugin sociales se encuentran señalados con el logo de la correspondiente red social.
		Cuando interactúa con un plugin en el Sitio Web (p.ej. con el botón “Me gusta”), o deja un comentario, la información correspondiente se envía por el navegador directamente a la plataforma de la Red Social (en nuestro ejemplo Facebook) y es memorizada por ésta.
		</p>
		<p>Por favor, consulte con la política de privacidad de la Red Social si quiere tener más información acerca de la finalidad, tipo y método de recogida de datos personales, tratamiento, utilización y almacenaje por parte de la plataforma de la Red Social, así como sobre información sobre cómo ejercer sus derechos.</p>
		<p>En este momento no hay ningún plugin social en el Sitio Web.</p>
		<h2>10. Enlaces desde/ a sitios web de terceros</h2>
		<p>Desde el Sitio Web puede utilizar enlaces para ir a otros sitios webs. También puede ser dirigido a este Sitio Web por terceros.</p>
		<p>El Responsable del Tratamiento declina cualquier responsabilidad por cualquier petición de datos personales u ofrecimiento de datos personales por sitios webs de terceros así como en relación con la autenticación de credenciales emitidas por terceros.</p>
		<p>Usted es responsable de revisar las políticas de privacidad de cualquier sitio web de terceros antes de facilitar cualquier información.</p>
		<h2>11. Tecnología utilizada</h2>
		<h2>Cookies</h2>
		<p>Este Sitio Web usa cookies y tecnologías similares (“Cookies”), incluyendo Cookies de terceros.</p>
		<p>Las Cookies son pequeños archivos de datos que se colocan en su ordenador, smartphone u otro dispositivo. Las Cookies se almacenan en el disco duro de su dispositivo sin riesgo de dañarlo.</p>
		<p>Al entrar en este Sitio Web usted puede:</p>
		<ul>
			<li>Aceptar el uso de Cookies; o</li>
			<li>Rechazar el uso de Cookies; o</li>
			<li>Configurar sus preferencias.</li>
		</ul>
		<p>Por favor, consulte nuestra Política de <b class="bold">Cookies</b> si quiere tener más información.</p>
		<h2>12. Cómo contactarnos</h2>
		<p>Si tiene cualquier pregunta o comentario acerca de esta Política de Privacidad, por favor envíe un e-mail al Delegado de Protección de Datos de Ipsen en: <a href="mailto:dataprivacy@ipsen.com" target="__blank">dataprivacy@ipsen.com</a></p>
	</div>

		</div><!-- #main -->

<?php
get_footer();
