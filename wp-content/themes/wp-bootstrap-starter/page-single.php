<?php
/*
Template Name: single
*/
 
 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="primary" class="home-container">
		<div id="main" class="site-main" role="main">
		
		<?php while (have_rows('posts_diarrea')) : the_row(); ?>
								
							<?php
							$the_link = get_permalink();
							$description = get_sub_field('description', 'option');
							$title = get_sub_field('title', 'option');
							$especialidad = get_sub_field('especialidad', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('categoria', 'option');
							?>
							<div class="title-icon-submenu">
								<p><?php echo ($description) ?> </p>
								</div>
						<div class="container-doctors-diarrea">
							<div class="dr-important">
									<div class="caja-img">
										<img src="" alt="">
									<div class="img-dr-important" style="background-image: url('<?php echo esc_url($image); ?>');">
									<i class="fa fa-play-circle"></i></div>
									<!-- <i class="fa fa-play-circle"></i> -->
								</div>
								<div class="caja-texto">
									<h2><?php echo ($title) ?> <a href="#post<?php echo $the_link; ?>"></a> </h2> <br>
									<p><?php echo ($name) ?></p> <br>
									<p><i><?php echo ($especialidad) ?></i></p>
							</div>
							</div>
							</div>
							<?php endwhile; ?>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php

get_footer();
