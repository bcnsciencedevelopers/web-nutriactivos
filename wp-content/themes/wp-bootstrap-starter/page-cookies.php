<?php

/*
Template Name: cookies
*/
 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

		<div id="main" class="site-main" role="main">

		<div class="container-terminos">
		<h1>Política de Cookies</h1> 
		<p>El objetivo de esta Política de Cookies es ilustrar el tipo de cookies que usa este Sitio Web, las formas en que éstas se usan, y cómo el usuario puede gestionarlas. </p>
		<h2>¿Qué es una cookie?</h2> 
		<p>Las cookies son pequeños ficheros de texto que se envían a su dispositivo cuando visita una página web, y se almacenan en el disco duro de su dispositivo.</p>
		<p>Algunas cookies son consideradas estrictamente necesarias: estas cookies se usan para facilitar tareas esenciales en el sitio web, como ayudar a los usuarios a navegar entre páginas de forma eficiente, haciendo que la página funcione de forma segura y eficaz. </p>
		<p>Otras cookies se usan para gestionar el rendimiento y trazar el uso del sitio web. Ipsen usa estas cookies en sus sitios web para entender qué uso se hace de estos, de forma que podamos mejorar y desarrollar nuestros sitios webs continuamente. </p>
		<p>De acuerdo con la Directiva EU 2009/136/EC, solo almacenaremos cookies en su dispositivo si es estrictamente necesario. Cuando usemos otras cookies que no son estrictamente necesarias, le pediremos permiso. </p>
		<p>Normalmente la información que se recoge usando cookies no le identifica directamente, pero algunas sí pueden contener datos personales, tales como la dirección IP, fecha y hora de la visita al sitio web, sistema operativo, navegador, y cómo el usuario accede al sitio web (ver la Política de Privacidad del Sitio Web para más información sobre cómo tratamos sus datos personales, sobre sus derechos y sobre cómo contactar con nosotros). </p>

		<h2>¿Qué cookies usa este Sitio Web?</h2>
		<p>Este Sitio Web usa cookies estrictamente necesarias para gestionar la navegación del sitio, así como para la funcionalidad y seguridad del usuario. </p>
		<p>El Sitio Web usa también cookies analíticas. Éstas facilitan a Ipsen información acerca del comportamiento del visitante del sitio web, tales como identificar palabras clave del motor de búsqueda que condujeron al usuario al sitio web, cuánto tiempo ha estado en el sitio web, qué páginas ha visitado y cuáles son las más visitadas. </p>
		<p>Este Sitio Web también usa Google Analytics para hacer seguimiento del uso del sitio y facilitar estadísticas generales sobre el uso del Sitio Web. Puede ver la información de privacidad de Google Analytics en el siguiente enlace: <a href="https://support.google.com/analytics/answer/6004245#zippy=%2Cour-privacy-policy">Safeguarding your data - Analytics Help (google.com)</a></p>

		<h2>¿Por cuánto tiempo se conserva la información recopilada de las cookies?</h2> 
		<p>Las cookies caducan cuando la información ya no es necesaria, o después de un periodo máximo de 13 meses.</p>
		
		<h2>¿Cómo configuro o cambio mi configuración de cookies?</h2>
		<p>Cuando entre por primera vez en este Sitio Web se le presentarán opciones para gestionar las cookies. También se le presentarán opciones periódicamente. Podrá aceptar todas las cookies o elegir qué cookies acepta o rechaza. Si rechaza todas las cookies éstas no se enviarán a su dispositivo. </p>
		<p>Alternativamente, puede controlar las cookies a través de la mayoría de navegadores en su configuración. Para saber más sobre cookies, incluyendo cómo saber qué cookies se han establecido, puede visitar <a href="www.aboutcookies.org">www.aboutcookies.org</a> o <a href="www.allaboutcookies.org">www.allaboutcookies.org</a>. Para no ser rastreado por Google Analytics en todos los sitios web visite <a href="http://tools.google.com/dlpage/gaoptout">http://tools.google.com/dlpage/gaoptout</a>. </p>

		
		<h2>¿Qué ocurre cuando comparto en redes sociales? </h2>
		<p>Si “comparte” contenido de Ipsen en redes sociales, tales como Facebook y Twitter, puede que desde éstas le envíen cookies. No controlamos la configuración de estas cookies, por lo que le rogamos que compruebe con estos sitios web la información sobre cómo gestionar las mismas.</p>

		</div>

		</div><!-- #main -->

<?php
get_footer();
