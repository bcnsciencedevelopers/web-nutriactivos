<?php
/*
Template Name: diarrea
*/
 
 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="primary" class="home-container">
		<div id="main" class="site-main" role="main">
		
		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
				
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php
			endif;
			
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				// get_template_part( 'template-parts/content-slider', get_post_format() );
				get_template_part( 'template-parts/content-menu-ok', get_post_format() );
				get_template_part( 'template-parts/content-diarrea', get_post_format() );
		
					
				?>
				
				<div class="container-doctors-diarrea">
				<!-- MOSTRAR POST -->
				<?php
     
					$args = array( 
						'post_type' => 'post', 
						'category_name' => 'diarrea_destacado', 
						// 'category_name' => array('diarrea+important'),
						// 'category_name' => array(['diarrea'] , ['important']), 
						// 'category_name' => array('diarrea , important'), 
						'orderby' => 'date', 
						'order' => 'DESC',
						'showposts' => 1);
				
					$wp_query = new WP_Query($args);
				
					if($wp_query->have_posts()) :
						while($wp_query->have_posts()) :
						$wp_query->the_post(); 
						
				?>
				
							<?php while (have_rows('posts_diarrea')) : the_row(); ?>
								
							<?php
							$the_link = get_permalink();
							$title = get_sub_field('title', 'option');
							$link = get_sub_field('link', 'option');
							$especialidad = get_sub_field('especialidad', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('categoria', 'option');
							$link_url = $link['url'];
							?>
							<div class="dr-important">
									<div class="caja-img">
										<img src="" alt="">
									<div class="img-dr-important" style="background-image: url('<?php echo esc_url($image); ?>');">
									
									<a href="<?php echo esc_url($link_url); ?>" target="__blank">
									<i class="fa fa-play-circle"></i>
									</a>
								</div>
									<!-- <i class="fa fa-play-circle"></i> -->
								</div>
								<div class="caja-texto">
									<h2><?php echo ($title) ?> <a href="#post<?php echo $the_link; ?>"></a> </h2> <br>
									<p><?php echo ($name) ?></p> <br>
									<p><i><?php echo ($especialidad) ?></i></p>
							</div>
							</div>
							<?php endwhile; ?>
				
				
					<?php   endwhile; endif; 
					wp_reset_query(); 
					
				?>
				<!-- MOSTRAR POST -->
				<?php
     
					$args = array( 
						'post_type' => 'post', 
						'category_name' => 'diarrea', 
						// 'category_name' => array('diarrea+important'),
						// 'category_name' => array(['diarrea'] , ['important']), 
						// 'category_name' => array('diarrea , important'), 
						'orderby' => 'date', 
						'order' => 'ASC',
						'showposts' => 10);
				
					$wp_query = new WP_Query($args);
				
					if($wp_query->have_posts()) :
						while($wp_query->have_posts()) :
						$wp_query->the_post(); 
						
				?>
				
							<?php while (have_rows('posts_diarrea')) : the_row(); ?>
								
							<?php
							$title = get_sub_field('title', 'option');
							$especialidad = get_sub_field('especialidad', 'option');
							$image = get_sub_field('image', 'option');
							$name = get_sub_field('name', 'option');
							$categoria = get_sub_field('categoria', 'option');
							$link = get_sub_field('link', 'option');

							?>
							<div class="drs-diarrea-container">
								<div class="dr-diarrea" >
									<div class="dr-diarrea-img-posts" style="background-image: url('<?php echo esc_url($image); ?>');">
									</div>
									<div class="dr-diarrea-text-posts">
									<h2><?php echo ($title) ?></h2> <br>
									<p><?php echo ($name) ?></p> <br>
									<p><i><?php echo ($especialidad) ?></i></p> <br>
									
									<button class="ver-mas"><a href="<?php echo get_permalink(); ?>">Ver más</a></button>
									
									</div>
								</div>

							</div>
							
							<?php endwhile; ?>
				
				
					<?php   endwhile; endif; 
					wp_reset_query(); 
					
				?>
				<div class="volver">
				<button class="volver-btn"><a href="<?php echo esc_url( home_url( '/' )); ?>">← volver</a></button>
				</div>			
			</div>
				<?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				// get_template_part( 'template-parts/content', get_post_format() );
					
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>


		</div><!-- #main -->

	</div><!-- #primary -->

<?php

get_footer();
