<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://wordpress.org/support/article/editing-wp-config-php/

 *

 * @package WordPress

 */


define( 'WP_SITEURL', 'http://localhost/nutriactivos_local' ); 
define( 'WP_HOME', 'http://localhost/nutriactivos_local' );
 define('RELOCATE',true); 


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', "nutriactivoslocal" );


/** MySQL database username */

define( 'DB_USER', "root" );


/** MySQL database password */

define( 'DB_PASSWORD', "" );


/** MySQL hostname */

define( 'DB_HOST', "localhost" );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8mb4' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         'vO#4$zp@9vN<-$(d]?1Qv/H`BRwSuBZimgSb9/ cxaKhtYhg;QVHl`!)Tl;H:MMK' );

define( 'SECURE_AUTH_KEY',  '|opb{bJ^f[m+^rikX*DC.@GFe_wUDl9f<# Vn~svi;m3$*q#oSUR58{K+P!{+F|)' );

define( 'LOGGED_IN_KEY',    'wz2_i?pZ[^*nc+5<Z~eQS!)^*LT P8miMZN-Jj{C5fLjlG}pj++H{0uw)f9T>/c<' );

define( 'NONCE_KEY',        'd-:}ZV!_wz*lvvf$-U,#5MF:6%9rg2s)BuPJ|F5Fz8-i]k&0994`lTiZE{T-bEtj' );

define( 'AUTH_SALT',        'm~vRSid7R[b<Q@<Yw+V4[Fz}FJ-ns%[Cf8{u^X0/#;Zx|xTxv{6%sJzc=B$3V>%2' );

define( 'SECURE_AUTH_SALT', 'q:g]f7PL_h8Lf]xFJ%orj@3_FmjS25SD%%y2=b)[p+rcK{rJR.Z/ap`6]D!dK.}I' );

define( 'LOGGED_IN_SALT',   'CE;=H?6eP3>STI`PLU;yuBY9:JKab*v*`fxEI=UE{4:eykScn/y?z+?q`LO3(<5?' );

define( 'NONCE_SALT',       '9YW-Ty16r~(}d X*.-?P$/(]HVCztd%Ge_oG=s-`DN$`n>t_`tV1<`GoD+=/h)Qt' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the documentation.

 *

 * @link https://wordpress.org/support/article/debugging-in-wordpress/

 */

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', __DIR__ . '/' );

}


/** Sets up WordPress vars and included files. */

require_once ABSPATH . 'wp-settings.php';

/* Desactivar el uso de FTP para actualizar Plugins y Themes en WordPress */
define('FS_METHOD','direct');

